seedmediatechtest
=====================

A Symfony project created on November 7, 2017, 11:49 pm.

in order to run it the following commands needs to be executed:

```
composer install
```

```
SQL:
CREATE TABLE `seedmediatechtest`.`articles` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `guid` VARCHAR(45) NOT NULL,
  `slug` VARCHAR(100) CHARACTER SET 'utf8mb4' NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `summary` VARCHAR(255) NOT NULL,
  `body` MEDIUMTEXT NOT NULL,
  `date_published` DATETIME NOT NULL,
  `last_scraped_date` DATETIME NOT NULL,
  `status` TINYINT NOT NULL,
  `remote_id` VARCHAR(255) NOT NULL,
  `hash` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `guid` (`guid` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci
COMMENT = 'Contains articles, indexed by GUID';
```

Please edit app/config/parameters.yml for DB credentials

to trigger the process:
```
bin/console cron:fetch:articles
```
