<?php

namespace AppBundle\DependencyInjection;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This class is automatically discovered by the kernel and load() is called at startup.
 */
class AppExtension extends Extension {

    /**
     * Called by the kernel at load-time.
     */
    public function load(array $configs, ContainerBuilder $container) {
    	$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);
		$container->setParameter('config', $config);
    }
}