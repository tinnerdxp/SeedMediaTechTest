<?php

namespace AppBundle\Services\Scraper\Source;

use AppBundle\Entity\ArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;

class SlashdotScraperSource extends BaseAbstract {
    const URL = 'https://slashdot.org';

    private function getClient(): Client {
        return $this->getDependency('client');
    }

    private function fetch(string $url): Response {
        return $this->getClient()->get($url);
    }

    public function getArticleList(\DateTime $fromDate, \DateTime $toDate, int $limit = self::DEFAULT_LIMIT): Collection {
        $body = (string) $this->fetch(self::URL)->getBody();

        /** @var Crawler $crawler */
        $crawler = $this->getDependency('crawler');
        $crawler->clear();
        $crawler->addHtmlContent($body);

        $container = new ArrayCollection();
        $crawler->filterXPath('//article')->each(function($node, $i) use ($container) {
            $uniqueId = (int) $node->attr('data-fhid');

            // end of feed
            if ($uniqueId === 0) {
                return;
            }

            $title= $node->filter('*[class="story-title"]>a')->text();
            $summary = trim($node->filter('*[class="body"]')->text(), "\n\t\r ");
            $time = str_replace(['on ', '@'], ['',''], $node->filter('time')->attr('datetime'));
            $publicationDate = new \DateTime('@' . strtotime($time));
            $article = $this->buildArticleEntity($uniqueId, $title, $summary, $summary, $publicationDate);
            $container->set($uniqueId, $article);
        });
        return $container;
    }

    public function getArticleByRemoteId(string $id): ArticleEntity {
        // TODO: Implement getArticleByRemoteId() method.
    }

}

