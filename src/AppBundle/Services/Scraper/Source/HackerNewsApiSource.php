<?php

namespace AppBundle\Services\Scraper\Source;

use AppBundle\Entity\ArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class HackerNewsApiSource extends BaseAbstract {
    const SINGLE_ITEM_URL = 'https://hacker-news.firebaseio.com/v0/item/<id>.json';
    const MAX_ITEM_ID_URL = 'https://hacker-news.firebaseio.com/v0/maxitem.json';
    const NEW_STORIES_URL = 'https://hacker-news.firebaseio.com/v0/newstories.json';
    const ASK_STORIES_URL = 'https://hacker-news.firebaseio.com/v0/askstories.json';

    private function getClient(): Client {
        return $this->getDependency('client');
    }

    private function fetch(string $url): Response {
        return $this->getClient()->get($url);
    }

    private function getNewStoriesIds() {
        return \GuzzleHttp\json_decode((string) $this->fetch(self::ASK_STORIES_URL)->getBody());
    }

    private function getMaxId(): int {
        $response =  $this->fetch(self::MAX_ITEM_ID_URL);
        $maxIdString = (string) $response->getBody();
        return (int) $maxIdString;
    }

    public function getArticleList(\DateTime $fromDate, \DateTime $toDate, int $limit = self::DEFAULT_LIMIT): Collection {
        $newStoriesIds = $this->getNewStoriesIds();
        $container = new ArrayCollection();
        foreach($newStoriesIds as $id) {
            try {
                $article = $this->getArticleByRemoteId($id);
            } catch (\InvalidArgumentException $e) {
                continue;
            }

            $container->set($id, $article);
        }
        return $container;
    }

    public function getArticleByRemoteId(string $id): ArticleEntity {
        $url = str_replace('<id>', $id, self::SINGLE_ITEM_URL);
        $json = \GuzzleHttp\json_decode($this->fetch($url)->getBody());

        // for the purpose of the test we are going to skip any posts without the "text" property
        if (empty($json->text)) {
            throw new \InvalidArgumentException('no text field');
        }
        return $this->buildArticleEntity($id, $json->title, null, $json->text, new \DateTime('@' . $json->time));
    }

}