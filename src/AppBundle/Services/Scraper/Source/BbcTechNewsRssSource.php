<?php

namespace AppBundle\Services\Scraper\Source;

use AppBundle\Entity\ArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;

class BbcTechNewsRssSource extends BaseAbstract {
    const FEED_URL = 'http://feeds.bbci.co.uk/news/technology/rss.xml';

    private function getClient(): Client {
        return $this->getDependency('client');
    }

    private function fetch(string $url): Response {
        return $this->getClient()->get($url);
    }

    public function getArticleList(\DateTime $fromDate, \DateTime $toDate, int $limit = self::DEFAULT_LIMIT): Collection {
        $body = (string) $this->fetch(self::FEED_URL)->getBody();

        /** @var Crawler $crawler */
        $crawler = $this->getDependency('crawler');
        $crawler->clear();
        $crawler->addXmlContent($body);

        $container = new ArrayCollection();
        $crawler->filterXPath('//item')->each(function($node, $i) use ($container) {
            $uniqueId = $node->filter('guid')->text();
            $title = $node->filter('title')->text();
            $summary = $node->filter('description')->text();
            $publicationDate = new \DateTime('@' . strtotime($node->filter('pubDate')->text()));
            $article = $this->buildArticleEntity($uniqueId, $title, $summary, $summary, $publicationDate);
            $container->set($uniqueId, $article);
        });
        return $container;
    }

    public function getArticleByRemoteId(string $id): ArticleEntity {
        // TODO: Implement getArticleByRemoteId() method.
    }
}
