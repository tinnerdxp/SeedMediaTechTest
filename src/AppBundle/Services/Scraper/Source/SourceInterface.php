<?php

namespace AppBundle\Services\Scraper\Source;

use AppBundle\Entity\ArticleEntity;
use Doctrine\Common\Collections\Collection;

interface SourceInterface {
    const DEFAULT_LIMIT = 100;
    public function getArticleList(\DateTime $fromDate, \DateTime $toDate, int $limit = self::DEFAULT_LIMIT): Collection;
    public function getArticleByRemoteId(string $id): ArticleEntity;
}