<?php

namespace AppBundle\Services\Scraper\Source;

use AppBundle\Entity\ArticleEntity;
use Cocur\Slugify\Slugify;

abstract class BaseAbstract implements SourceInterface {
    protected $config;
    protected $dependencies;
    public function __construct(array $config, array $dependencies) {
        $this->config = $config;
        $this->dependencies = $dependencies;
    }

    protected function getDependency(string $key) {
        return isset($this->dependencies[$key]) ? $this->dependencies[$key] : false;
    }

    public function buildArticleEntity(string $uniqueId, string $title, $summary = null, string $body, \DateTime $datePublished): ArticleEntity {
        /** @var Slugify $slugify */
        $slugify = $this->getDependency('slugify');

        /** @var ArticleEntity $article */
        $article = new ArticleEntity();

        /** @var \DateTime $now */
        $now = new \DateTime();

        // populate all the fields of the article
        $article->setSlug($slugify->slugify($title));
        $article->setTitle($title);
        if ($summary !== null) {
            $article->setSummary($summary);
        } else {
            $article->setSummary(substr($body, 0, 250) . '...');
        }
        $article->setBody($body);
        $article->setDatePublished($datePublished);
        $article->setLastScrapedDate($now);
        $article->setRemoteId($uniqueId);
        $article->setHash($this->getHash($title, $body));
        $article->setGuid(sha1($title . time()));
        $article->setStatus(1);
        return $article;
    }

    /**
     * Ideally this would be abstracted out in a separate service which could process the text using some fancy way.
     *
     * @param $title
     * @param $body
     */
    private function getHash($title, $body): string {
        /** @var Slugify $slugify */
        $slugify = $this->getDependency('slugify');
        $text = $slugify->slugify($title . '-' . $body);
        $parts = explode('-', $text);
        $considered = array_merge(array_slice($parts, 0, 10), array_slice($parts, count($parts), -10));
        $flattened = implode('', $considered);
        $md5 = md5($flattened);
        return $md5;
    }

}