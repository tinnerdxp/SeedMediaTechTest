<?php

namespace AppBundle\Services\Scraper;

use AppBundle\Services\Scraper\Source\SourceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\DependencyInjection\Container;

class ScraperService {
    protected $config;
    protected $container;

    public function __construct(array $config, Container $container) {
        $this->config = $config;
        $this->container = $container;
    }

    private function getSources(): array {
        $sources = [];
        foreach($this->config['scraper']['sources'] as $scraperServiceName) {
            $sources[] = $this->container->get($scraperServiceName);
        }
        return $sources;
    }

    public function scrapeAll(): Collection {
        // container to be returned
        $collection = new ArrayCollection();

        // prepare the params for each source
        $fromDate = new \DateTime($this->config['scraper']['from_date']);
        $toDate   = new \DateTime($this->config['scraper']['to_date']);
        $limit    = (int) $this->config['scraper']['limit'];

        /** @var SourceInterface $source */
        $sources = $this->getSources();
        foreach($sources as $source) {
            $articles = $source->getArticleList($fromDate, $toDate, $limit);
            $collection = new ArrayCollection($collection->toArray() + $articles->toArray());
        }
        return $collection;
    }
}