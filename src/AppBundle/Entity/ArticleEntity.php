<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="seedmediatechtest.articles")
 */
class ArticleEntity {
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="guid", type="string", length=100, unique=true, nullable=false)
     */
    private $guid;

    /**
     * @ORM\Column(name="slug", type="string", length=255, unique=false, nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(name="title", type="string", length=255, unique=false, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(name="summary", type="string", length=255, unique=false, nullable=false)
     */
    private $summary;

    /**
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    private $body;

    /**
     * @ORM\Column(name="date_published", type="datetime", nullable=false)
     */
    private $date_published;

    /**
     * @ORM\Column(name="last_scraped_date", type="datetime", nullable=false)
     */
    private $last_scraped_date;

    /**
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status = self::STATUS_ENABLED;

    /**
     * @ORM\Column(name="remote_id", type="string", length=255, nullable=false, unique=false)
     */
    private $remote_id;

    /**
     * @ORM\Column(name="hash", type="string", length=32, nullable=false, unique=false)
     */
    private $hash;

    /**
     * @return mixed
     */
    public function getId(): int {
        return (int) $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGuid(): string {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     */
    public function setGuid($guid) {
        $this->guid = $guid;
    }

    /**
     * @return mixed
     */
    public function getSlug(): string {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug(string $slug) {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSummary(): string {
        return $this->summary;
    }

    /**
     * @param mixed $summary
     */
    public function setSummary(string $summary) {
        $this->summary = substr($summary, 0, 255);
    }

    /**
     * @return mixed
     */
    public function getBody(): string {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody(string $body) {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getDatePublished(): \DateTime {
        return $this->date_published;
    }

    /**
     * @param mixed $date_published
     */
    public function setDatePublished(\DateTime $date_published) {
        $this->date_published = $date_published;
    }

    /**
     * @return mixed
     */
    public function getLastScrapedDate(): \DateTime  {
        return $this->last_scraped_date;
    }

    /**
     * @param mixed $last_scraped_date
     */
    public function setLastScrapedDate(\DateTime $last_scraped_date) {
        $this->last_scraped_date = $last_scraped_date;
    }

    /**
     * @return mixed
     */
    public function getStatus(): int {
        return (int) $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus(int $status) {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getRemoteId() {
        return $this->remote_id;
    }

    /**
     * @param mixed $remote_id
     */
    public function setRemoteId(string $remoteId) {
        $this->remote_id = $remoteId;
    }

    /**
     * @return mixed
     */
    public function getHash(): string {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash(string $hash) {
        $this->hash = $hash;
    }
}
