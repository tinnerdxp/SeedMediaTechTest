<?php
namespace AppBundle\Command;

use AppBundle\Entity\ArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FetchArticlesCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('cron:fetch:articles');
        $this->setDescription('Runs the importer');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $scraper = $this->getContainer()->get('service.scraper');

        /** @var ArrayCollection $collection */
        $collection = $scraper->scrapeAll();

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $articlesRepository = $em->getRepository('AppBundle:ArticleEntity');

        /** @var ArticleEntity $item */
        foreach($collection as $item) {
            // slowest possible way of doing this, but uses least memory at this point
            if (empty($articlesRepository->findBy(['hash' => $item->getHash()]))) {
                $em->persist($item);
            }
        }
        $em->flush();
        $output->writeln('All done');
    }
}
