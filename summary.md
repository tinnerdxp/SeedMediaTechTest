Please note the architecture used in the test code is simplified as it would take considerably longer to implemented the ideal one described below.

**Ideal architecture overview:**
1. The Service layer would remain as is
2. The Source folder would contain more generic modules which would be fully configurable.
3. Each combination of modules along with their configuration would then be stored in either a DB, Consul service, config file or bundle configuration.
4. I have used GuzzleHttp client on purpose as it supports concurrent requests with Promises. This would allow really good scaling.
5. The process would be triggered by cron scanning or fetching remote "list" of resources and inserting normalised or raw data into the queueing system - such as Rabbit MQ
6. Another cron process would process the queue fetching individual items and insert parsed data into the DB. This can also dedupe articles based on variety of external rules. Processing the queue by a single process isolated from heavy load by the scanner allows for safe writes into the DB.
7. This approach would scale really well and would be pretty fool-prof.

Current architecture overview:
1. I have used a Symfony framework to develop a Command that would be triggered by cron or manually
2. The Scraper service is being used and it uses individual classes for each source.
3. The sources are configured in the bundle's config file, but all the configuration is currently hardcoded in the classes for simplicity
4. There is no queing system, so the whole process is synchronous and is not blocking itself which could lead to potential problems, multiple instances triggering at the same time.
5. I have created the DB structure before looking at the data so some fields are either truncated or duplicated for the purpose of this test.
6. I haven't written any automated tests so far, but for this kind of task majority of them would be unit and functional. I would not use acceptance tests for this type task.
7. Ideally this code should be treated as a prototype where further refactoring should take place.

**Notes:**

- The coding style is very subjective but luckily, it's usually just a setting in modern IDEs.
- The ability to configure variety of sources hasn't been implemented as it's really time consuming to build a parser for every html / xml document out there.
- I haven't used github, as my current company uses github as well and they would have noticed me having a public repository, hence why my personal gitlab was used.
- Deduping is sort of done - the implementation is very simple though, but as long as the hash method is implemented it should dedupe articles
- The test took me 2.5h, starting from an empty folder, additinal 30 minutes was spent on commenting the code and writing this file

**How to expand**
- The obvious step forward would be to generalize the modules fetching content from external sources, they should really be of different types with rich configuration rather than hardcoded classes.
- Then queueing as described above to make the process safe for DB writes under heavy load and plenty of sources
- Concurrency using promises should be used. Further scaling step would be to introduce additional queue with URLs to be scanned, the process pulling from this queue could be off-loaded to dedicated microservice that could be running on thousands of nodes.
- More clever algorithm to dedupe content and change the DB structure to allow for multiple sources for individual article (if necessary)
- Better filtering of content and perhaps support for storing snapshots of remote content
- API to retrieve the content

**_Please refer to README.md for required SQL schema_**